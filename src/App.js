import React, {useState} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Quiz from "./components/question";
import Admin from "./components/admin";
import Login from "./components/login";


const App = () => {

    return (
        <Router>
            <React.Fragment>
                <Switch>
                    <Route exact path={'/'} component={Quiz}/>
                    <Route exact path={'/admin-panel'} component={Admin}/>
                    <Route exact path={'/login'} component={Login}/>
                </Switch>
            </React.Fragment>
        </Router>
    )
};

export default App;