import React, {useEffect, useState} from 'react';
import {request} from 'graphql-request';
import Moment from 'react-moment';
import { ExportToCsv } from 'export-to-csv';
import Login from "../login";


const options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalSeparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'My Awesome CSV',
    useTextFile: false,
    useBom: true,
    useKeysAsHeaders: false,
    // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
};

const csvExporter = new ExportToCsv(options);





const Admin = () => {
    const [token, setToken] = useState();
    const [adminData, setAdminData] = useState(null);
    const [popup, setPopup] = useState(null);

    let cvsData = [];

    const changeStyle = () => {
        document.body.classList.add('dark');
    }

    useEffect(() => {
        changeStyle();

        const fetchProducts = async () => {
            const {quizAnswersID} = await request(
                'https://api-eu-central-1.graphcms.com/v2/ckkkz3ey6710d01xsajjdcgl2/master',
                `
                      {
                        quizAnswersID {
                        question
                        answersData
                        createdAt
                        id
                        userName
                        userEmail
                      }
                    }
                    `
            );
            setAdminData(quizAnswersID);
        };

        fetchProducts();


    }, []);

    const generateCVS = () => {

        for (let i = 0; i < adminData[0].question.length; i++) {
            let answerT = adminData[0].question[i];
            console.log(answerT);
            cvsData.push({answerT});
        }
        console.log(cvsData);
        csvExporter.generateCsv(cvsData);
    }

    /*if(!token) {
        return <Login setToken={setToken} />
    }*/

    const setPopupActive = (i) => {
        if (popup !== i) {
            setPopup(i);
        } else {
            setPopup(null)
        }
    }


    const deleteAnswer = async (id) => {
        await request(
            'https://api-eu-central-1.graphcms.com/v2/ckkkz3ey6710d01xsajjdcgl2/master',
            `
                     mutation {
                      deleteQuizAnswers(where: {id: "${id}"}) {
                        id
                      }
                    }
                    `
        );
        window.location.reload();
    };

    return (
        <div className="admin-panel">
            {!adminData ? (
                'Loading'
            ) : (
                <React.Fragment>
                    <div className="admin-title">
                        Admin panel
                    </div>
                    {!adminData.length > 0 ? (
                        'There are no answers yet'
                    ) : (
                        <div className="admin-box">
                            {console.log(adminData)}
                            <div className="answers-list">
                                {
                                    adminData.map(({question, answersData, createdAt, id, userName, userEmail}, i) => (
                                        <div className="answers-list--item">
                                            <div className="wrap">
                                                <div className="answers-list--number">
                                                    <span>{i + 1}</span>
                                                </div>
                                                <div className="answers-list--date">
                                                    <Moment fromNow>{createdAt}</Moment>
                                                </div>
                                                <button
                                                    className="btn"
                                                    onClick={() => setPopupActive(i)}
                                                >
                                                    See answers
                                                </button>
                                                <button
                                                    className="btn"
                                                    onClick={() => deleteAnswer(id)}
                                                >
                                                    Delete answer
                                                </button>
                                            </div>


                                            <div
                                                className={`answer-popup ${i === popup ? 'is-shown' : 'is-hidden'}`}
                                            >
                                                <>
                                                    <div className="user-info">
                                                        <p><b>Name:</b>{userName}</p>
                                                        <p><b>Email:</b>{userEmail}</p>
                                                    </div>
                                                {
                                                    answersData.map((item, i) => (
                                                        <div className="answers-popup--item">
                                                            <span>{i + 1}</span>
                                                            <div className="answer_wrap">
                                                                <b>{question[i]}</b>
                                                                <p>{item}</p>
                                                            </div>

                                                        </div>
                                                    ))
                                                }
                                                </>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                            {/*<button
                                className="btn"
                                onClick={generateCVS}
                            >
                                Generate CVS file
                            </button>*/}
                        </div>
                    )}

                </React.Fragment>
            )}
        </div>
    )
}

export default Admin;