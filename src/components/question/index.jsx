import React, {useEffect, useState} from 'react';
import {request} from 'graphql-request';

function Quiz() {
    const [questions, setQuestions] = useState(null);
    const [radioButton, setRadioButton] = useState([]);
    const [message, setMessage] = useState([]);
    const [dataInfo, setDataInfo] = useState([]);
    const [questionsInfo, setQuestionsInfo] = useState([]);
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [showQuiz, setShowQuiz] = useState(false);

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [thankYou, setThankYou] = useState(false);
    const [finishQuiz, setFinishQuiz] = useState(false);


    const onRadioChange = (value) => {
        setRadioButton(radioButton.concat(value));
    }

    const onTextChange = (e) => {
        setMessage([]);
        setMessage([e.target.value]);
    }

    const onNameChange = (e) => {
        setName([]);
        setName([e.target.value]);
    }

    const onEmailChange = (e) => {
        setEmail([]);
        setEmail([e.target.value]);
    }

    const startQuiz = (e) => {
        e.preventDefault();
        setShowQuiz(true);
    }


    useEffect(() => {
        const fetchProducts = async () => {
            const {questionsID} = await request(
                'https://api-eu-central-1.graphcms.com/v2/ckkkz3ey6710d01xsajjdcgl2/master',
                `
                      {
                        questionsID {
                            question
                            answer
                            questionType
                          }
                    }
                    `
            );
            setQuestions(questionsID);
        };

        fetchProducts();


    }, []);


    const pushAnswer = async () => {

        setThankYou(true);

        await request(
            'https://api-eu-central-1.graphcms.com/v2/ckkkz3ey6710d01xsajjdcgl2/master',
            `
                mutation {
                  createQuizAnswers(data: {question: [${questionsInfo}] , answersData: [${dataInfo}], userName: "${name}" , userEmail: "${email}" }) {
                    question
                    answersData
                    userName
                    userEmail
                  }
                }
            `
        );

    }


    const handleAnswerOptionClick = (question, value) => {
        const nextQuestion = currentQuestion + 1;
        setDataInfo(dataInfo.concat(`"${value}"`));
        setQuestionsInfo(questionsInfo.concat(`"${question}"`));
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setFinishQuiz(true);
        }
    };


    return (
        <div className="app">
            {!questions ? (
                'Loading'
            ) : (
                <React.Fragment>

                    <div className='main-wrap'>
                        {finishQuiz ? (
                            <div className='score-section'>
                                <button
                                    className={`finish ${thankYou ? 'is-hidden' : ''}`}
                                    onClick={() => pushAnswer(questionsInfo, dataInfo)}
                                >Finish quiz
                                </button>
                                {thankYou ? (
                                    <span>Thank you for answering our survey! Your feedback is greatly appreciated.</span>
                                ) : (<></>)
                                }

                            </div>
                        ) : (
                            <>
                                <div className={`user-box ${showQuiz ? 'is-hidden' : 'is-shown'}`}>
                                    <form onSubmit={startQuiz}>
                                        <input type="text"
                                               placeholder="Your name"
                                               value={name}
                                               onChange={onNameChange}
                                               required="true"
                                        />
                                        <input type="email"
                                               placeholder="Your Email"
                                               value={email}
                                               onChange={onEmailChange}
                                               required="true"
                                        />
                                        <button
                                            className="btn"
                                        >
                                            Start quiz
                                        </button>
                                    </form>
                                </div>
                                <div className={`question-box ${showQuiz ? 'is-shown' : 'is-hidden'}`}>
                                    <img src="https://purgingquiz.smacne.co/img/logo.png" alt=""/>
                                    <div className='question-section'>
                                        <div className='question-count'>
                                            <span>Question {currentQuestion + 1}</span> / {questions.length}
                                        </div>
                                    </div>


                                    <React.Fragment>
                                        {
                                            questions.map(({question, answer, questionType}, i) => (
                                                <div
                                                    className={`question-box__inner ${i === currentQuestion ? 'is-shown' : 'is-hidden'}`}>
                                                    <div className='question-text'>{question}</div>
                                                    <div className='answer-section'>
                                                        {
                                                            (() => {
                                                                switch (questionType) {
                                                                    case "radio":
                                                                        return (
                                                                            <div className="radio-wrap">
                                                                                {answer.map(
                                                                                    (item, i) =>
                                                                                        <div
                                                                                            className="radio-input"
                                                                                        >
                                                                                            <input
                                                                                                type="radio"
                                                                                                onChange={() => onRadioChange(item)}
                                                                                            />
                                                                                            <label
                                                                                            >{item}</label>
                                                                                        </div>
                                                                                )}

                                                                                <button
                                                                                    onClick={() => handleAnswerOptionClick(question, radioButton)}
                                                                                >Next
                                                                                </button>
                                                                            </div>
                                                                        );
                                                                    case "message":
                                                                        return (
                                                                            <React.Fragment>
                                                                                    <textarea
                                                                                        placeholder="Type your message"
                                                                                        value={message}
                                                                                        onChange={onTextChange}
                                                                                    />
                                                                                <button
                                                                                    onClick={() => handleAnswerOptionClick(question, message)}
                                                                                >Next
                                                                                </button>
                                                                            </React.Fragment>
                                                                        )
                                                                    default:
                                                                        return answer.map(
                                                                            (item, i) =>
                                                                                <button
                                                                                    onClick={() => handleAnswerOptionClick(question, item)}
                                                                                >{item}</button>
                                                                        );
                                                                }
                                                            })()

                                                        }
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </React.Fragment>


                                </div>
                            </>
                        )}
                    </div>

                </React.Fragment>
            )}
        </div>
    );
}

export default Quiz;