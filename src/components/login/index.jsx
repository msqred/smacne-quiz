import React, {useEffect, useState} from 'react';
import {request} from "graphql-request";


export default function Login({setToken}) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [userData, setUserData] = useState();

    const getUser = async (login, password) => {
        const {usersID} = await request(
            'https://api-eu-central-1.graphcms.com/v2/ckkkz3ey6710d01xsajjdcgl2/master',
            `
                      {
                      usersID (where: {login: "${login}", password: "${password}"}) {
                        login
                        password
                        id
                        token
                      }
                    }
                    `
        );
        return usersID;
    }


    const onHandleSubmit = e => {
        e.preventDefault();
        getUser(username, password).then((value) => {
            console.log(value);
            if (value.length > 0) {
                console.log(value[0].id);
                setToken(value[0].id);
            } else {
                alert('Password is incorrect')
            }
        });
    }


    return (
        <form onSubmit={onHandleSubmit}>
            <label>
                <p>Username</p>
                <input type="text" onChange={e => setUserName(e.target.value.toString())}/>
            </label>
            <label>
                <p>Password</p>
                <input type="password" onChange={e => setPassword(e.target.value.toString())}/>
            </label>
            <div>
                <button type="submit">Submit</button>
            </div>
        </form>
    )
}
